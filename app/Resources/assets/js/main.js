jQuery(document).ready(function($) {
    var animation_speed = 250;
    $('.main-header-menu .navbar-nav > li').mouseover(function(){
        $(this).addClass('hover').siblings().removeClass('hover');
        var positionLeft = parseFloat($(this).position().left) + parseFloat($(this).outerWidth(true) - $(this).outerWidth());
        $('.navbar-active-indicator-container .navbar-active-indicator').stop().animate({
            width: $(this).outerWidth(),
            left: positionLeft,
            bottom: 0
        },{
            duration: animation_speed,
            queue: false
        });
    }).mouseout($.debounce(1000, function() {
        $('.main-header-menu .navbar-nav > li.active').trigger('mouseover');
    }));
    $('.main-header-menu .navbar-nav > li.active').trigger('mouseover');

    $('.scroll-down-indicator').click(function(e){
        $('html, body').animate({
            scrollTop: $('.header-container').offset().top
        },1000);
    });
});