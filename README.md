steviostudio7
=============

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/dbbb85b3-735f-4027-95cd-367fb3512061/big.png)](https://insight.sensiolabs.com/projects/dbbb85b3-735f-4027-95cd-367fb3512061)

Dopo aver clonato o pullato il repository eseguire composer install quindi controllare che la macchina locale sia configurata correttamente per eseguire symfony attraverso il comando php bin/symfony_requirements ed eventualemente risolvere i problemi. Quando scaricate il repository per la prima volta è importante impostare i permessi sulla cartella della cache ( vedi qui http://symfony.com/doc/current/setup/file_permissions.html ).

Per allineare il database locale eseguire i seguenti comandi:
- `php bin/console doctrine:database:drop --force` (eliminerà lo schema configurato nel vostro parameters.yml)
- `php bin/console doctrine:database:create` (creerà lo schema vuoto)
- `php bin/console doctrine:schema:update --force` (aggiornerà la struttura dello schema in base al mapping di doctrine)
- `php bin/console doctrine:fixtures:load -n`(popolerà il database con le fixture definite in src/AppBundle/DataFixtures/ORM)

risoluzione dei problemi che possono occorrere durante la procedura:
- se composer non è installato seguire queste istruzioni https://getcomposer.org/doc/00-intro.md per installarlo globalmente
- se `composer install` vi da problemi nel riscrivere la cartella cache seguire questa guida http://symfony.com/doc/current/setup/file_permissions.html

A Symfony project created on December 7, 2016, 4:55 pm.
