<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * PortfolioElement
 *
 * @ORM\Table(name="portfolio_element")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PortfolioElementRepository")
 */
class PortfolioElement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @ORM\ManyToMany(targetEntity="PortfolioCategory", inversedBy="portfolioElementArray")
     * @ORM\JoinTable(name="portfolio_element_on_portfolio_category")
     */
    private $portfolioCategoryArray;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Video", mappedBy="portfolioElement")
     */
    private $videoArray;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->videoArray = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     *
     * @return PortfolioElement
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return PortfolioElement
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return PortfolioElement
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return PortfolioElement
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Add videoArray
     *
     * @param \AppBundle\Entity\Video $videoArray
     *
     * @return PortfolioElement
     */
    public function addVideoArray(\AppBundle\Entity\Video $videoArray)
    {
        $this->videoArray[] = $videoArray;

        return $this;
    }

    /**
     * Remove videoArray
     *
     * @param \AppBundle\Entity\Video $videoArray
     */
    public function removeVideoArray(\AppBundle\Entity\Video $videoArray)
    {
        $this->videoArray->removeElement($videoArray);
    }

    /**
     * Get videoArray
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideoArray()
    {
        return $this->videoArray;
    }

    /**
     * Add portfolioCategoryArray
     *
     * @param \AppBundle\Entity\PortfolioCategory $portfolioCategoryArray
     *
     * @return PortfolioElement
     */
    public function addPortfolioCategoryArray(\AppBundle\Entity\PortfolioCategory $portfolioCategoryArray)
    {
        $this->portfolioCategoryArray[] = $portfolioCategoryArray;

        return $this;
    }

    /**
     * Remove portfolioCategoryArray
     *
     * @param \AppBundle\Entity\PortfolioCategory $portfolioCategoryArray
     */
    public function removePortfolioCategoryArray(\AppBundle\Entity\PortfolioCategory $portfolioCategoryArray)
    {
        $this->portfolioCategoryArray->removeElement($portfolioCategoryArray);
    }

    /**
     * Get portfolioCategoryArray
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPortfolioCategoryArray()
    {
        return $this->portfolioCategoryArray;
    }
}
