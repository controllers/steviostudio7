<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * PortfolioCategory
 *
 * @ORM\Table(name="portfolio_category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PortfolioCategoryRepository")
 */
class PortfolioCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @var int
     * @ORM\Column(name="sort_index", nullable=true, type="integer")
     */
    private $sortIndex;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="PortfolioElement", mappedBy="portfolioCategoryArray")
     */
    private $portfolioElementArray;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->portfolioElementArray = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return PortfolioCategory
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     *
     * @return PortfolioCategory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return int
     */
    public function getSortIndex()
    {
        return $this->sortIndex;
    }

    /**
     * @param int $sortIndex
     *
     * @return PortfolioCategory
     */
    public function setSortIndex($sortIndex)
    {
        $this->sortIndex = $sortIndex;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     *
     * @return PortfolioCategory
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     *
     * @return PortfolioCategory
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * Add portfolioElementArray
     *
     * @param \AppBundle\Entity\PortfolioElement $portfolioElementArray
     *
     * @return PortfolioCategory
     */
    public function addPortfolioElementArray(PortfolioElement $portfolioElementArray)
    {
        $this->portfolioElementArray[] = $portfolioElementArray;

        return $this;
    }

    /**
     * Remove portfolioElementArray
     *
     * @param \AppBundle\Entity\PortfolioElement $portfolioElementArray
     */
    public function removePortfolioElementArray(PortfolioElement $portfolioElementArray)
    {
        $this->portfolioElementArray->removeElement($portfolioElementArray);
    }

    /**
     * Get portfolioElementArray
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPortfolioElementArray()
    {
        return $this->portfolioElementArray;
    }

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        return $this->getTitle();
    }

}
