<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * BlogArticleSource
 *
 * @ORM\Table(name="blog_article_source")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BlogArticleSourceRepository")
 */
class BlogArticleSource
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="label", type="string", length=255)
     */
    private $label;

    /**
     * @var string
     * @ORM\Column(name="url", type="string")
     */
    private $url;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @var BlogArticle
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\BlogArticle", inversedBy="blogArticleSourceArray")
     */
    private $blogArticle;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return BlogArticleSource
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return BlogArticleSource
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     *
     * @return BlogArticleSource
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     *
     * @return BlogArticleSource
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * Set blogArticle
     *
     * @param \AppBundle\Entity\BlogArticle $blogArticle
     *
     * @return BlogArticleSource
     */
    public function setBlogArticle(\AppBundle\Entity\BlogArticle $blogArticle = null)
    {
        $this->blogArticle = $blogArticle;

        return $this;
    }

    /**
     * Get blogArticle
     *
     * @return \AppBundle\Entity\BlogArticle
     */
    public function getBlogArticle()
    {
        return $this->blogArticle;
    }
}
