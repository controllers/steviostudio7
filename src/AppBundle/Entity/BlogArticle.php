<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * BlogArticle
 *
 * @ORM\Table(name="blog_article")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BlogArticleRepository")
 */
class BlogArticle
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="sub_title", type="string", length=500, nullable=true)
     */
    private $subTitle;

    /**
     * @var string
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $article;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageAssetUrl;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\BlogArticleSource", mappedBy="blogArticle")
     */
    private $blogArticleSourceArray;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return BlogArticle
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     *
     * @return BlogArticle
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param string $article
     *
     * @return BlogArticle
     */
    public function setArticle($article)
    {
        $this->article = $article;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageAssetUrl()
    {
        return $this->imageAssetUrl;
    }

    /**
     * @param string $imageAssetUrl
     *
     * @return BlogArticle
     */
    public function setImageAssetUrl($imageAssetUrl)
    {
        $this->imageAssetUrl = $imageAssetUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     *
     * @return BlogArticle
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     *
     * @return BlogArticle
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * @param string $subTitle
     *
     * @return BlogArticle
     */
    public function setSubTitle($subTitle)
    {
        $this->subTitle = $subTitle;
        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->blogArticleSourceArray = new ArrayCollection();
    }

    /**
     * Add blogArticleSourceArray
     *
     * @param \AppBundle\Entity\BlogArticleSource $blogArticleSourceArray
     *
     * @return BlogArticle
     */
    public function addBlogArticleSourceArray(BlogArticleSource $blogArticleSourceArray)
    {
        $this->blogArticleSourceArray[] = $blogArticleSourceArray;

        return $this;
    }

    /**
     * Remove blogArticleSourceArray
     *
     * @param \AppBundle\Entity\BlogArticleSource $blogArticleSourceArray
     */
    public function removeBlogArticleSourceArray(BlogArticleSource $blogArticleSourceArray)
    {
        $this->blogArticleSourceArray->removeElement($blogArticleSourceArray);
    }

    /**
     * Get blogArticleSourceArray
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBlogArticleSourceArray()
    {
        return $this->blogArticleSourceArray;
    }

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        return $this->getSlug();
    }

}
