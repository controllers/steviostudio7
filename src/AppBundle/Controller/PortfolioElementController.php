<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PortfolioElement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Portfolioelement controller.
 *
 * @Route("admin/portfolioelement")
 */
class PortfolioElementController extends Controller
{
    /**
     * Lists all portfolioElement entities.
     *
     * @Route("/", name="admin_portfolioelement_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $portfolioElements = $em->getRepository('AppBundle:PortfolioElement')->findAll();

        return $this->render('portfolioelement/index.html.twig', array(
            'portfolioElements' => $portfolioElements,
        ));
    }

    /**
     * Creates a new portfolioElement entity.
     *
     * @Route("/new", name="admin_portfolioelement_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $portfolioElement = new Portfolioelement();
        $form = $this->createForm('AppBundle\Form\PortfolioElementType', $portfolioElement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($portfolioElement);
            $em->flush($portfolioElement);

            return $this->redirectToRoute('admin_portfolioelement_show', array('id' => $portfolioElement->getId()));
        }

        return $this->render('portfolioelement/new.html.twig', array(
            'portfolioElement' => $portfolioElement,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a portfolioElement entity.
     *
     * @Route("/{id}", name="admin_portfolioelement_show")
     * @Method("GET")
     */
    public function showAction(PortfolioElement $portfolioElement)
    {
        $deleteForm = $this->createDeleteForm($portfolioElement);

        return $this->render('portfolioelement/show.html.twig', array(
            'portfolioElement' => $portfolioElement,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing portfolioElement entity.
     *
     * @Route("/{id}/edit", name="admin_portfolioelement_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PortfolioElement $portfolioElement)
    {
        $deleteForm = $this->createDeleteForm($portfolioElement);
        $editForm = $this->createForm('AppBundle\Form\PortfolioElementType', $portfolioElement);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_portfolioelement_edit', array('id' => $portfolioElement->getId()));
        }

        return $this->render('portfolioelement/edit.html.twig', array(
            'portfolioElement' => $portfolioElement,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a portfolioElement entity.
     *
     * @Route("/{id}", name="admin_portfolioelement_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, PortfolioElement $portfolioElement)
    {
        $form = $this->createDeleteForm($portfolioElement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($portfolioElement);
            $em->flush($portfolioElement);
        }

        return $this->redirectToRoute('admin_portfolioelement_index');
    }

    /**
     * Creates a form to delete a portfolioElement entity.
     *
     * @param PortfolioElement $portfolioElement The portfolioElement entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PortfolioElement $portfolioElement)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_portfolioelement_delete', array('id' => $portfolioElement->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
