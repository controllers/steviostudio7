<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BlogArticleSource;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Blogarticlesource controller.
 *
 * @Route("blogarticlesource")
 */
class BlogArticleSourceController extends Controller
{
    /**
     * Lists all blogArticleSource entities.
     *
     * @Route("/", name="blogarticlesource_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $blogArticleSources = $em->getRepository('AppBundle:BlogArticleSource')->findAll();

        return $this->render('blogarticlesource/index.html.twig', array(
            'blogArticleSources' => $blogArticleSources,
        ));
    }

    /**
     * Creates a new blogArticleSource entity.
     *
     * @Route("/new", name="blogarticlesource_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $blogArticleSource = new Blogarticlesource();
        $form = $this->createForm('AppBundle\Form\BlogArticleSourceType', $blogArticleSource);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($blogArticleSource);
            $em->flush($blogArticleSource);

            return $this->redirectToRoute('blogarticlesource_show', array('id' => $blogArticleSource->getId()));
        }

        return $this->render('blogarticlesource/new.html.twig', array(
            'blogArticleSource' => $blogArticleSource,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a blogArticleSource entity.
     *
     * @Route("/{id}", name="blogarticlesource_show")
     * @Method("GET")
     */
    public function showAction(BlogArticleSource $blogArticleSource)
    {
        $deleteForm = $this->createDeleteForm($blogArticleSource);

        return $this->render('blogarticlesource/show.html.twig', array(
            'blogArticleSource' => $blogArticleSource,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing blogArticleSource entity.
     *
     * @Route("/{id}/edit", name="blogarticlesource_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, BlogArticleSource $blogArticleSource)
    {
        $deleteForm = $this->createDeleteForm($blogArticleSource);
        $editForm = $this->createForm('AppBundle\Form\BlogArticleSourceType', $blogArticleSource);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('blogarticlesource_edit', array('id' => $blogArticleSource->getId()));
        }

        return $this->render('blogarticlesource/edit.html.twig', array(
            'blogArticleSource' => $blogArticleSource,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a blogArticleSource entity.
     *
     * @Route("/{id}", name="blogarticlesource_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, BlogArticleSource $blogArticleSource)
    {
        $form = $this->createDeleteForm($blogArticleSource);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($blogArticleSource);
            $em->flush($blogArticleSource);
        }

        return $this->redirectToRoute('blogarticlesource_index');
    }

    /**
     * Creates a form to delete a blogArticleSource entity.
     *
     * @param BlogArticleSource $blogArticleSource The blogArticleSource entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(BlogArticleSource $blogArticleSource)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('blogarticlesource_delete', array('id' => $blogArticleSource->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
