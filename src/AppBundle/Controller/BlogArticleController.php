<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BlogArticle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Blogarticle controller.
 */
class BlogArticleController extends Controller
{
    /**
     * Lists all blogArticle entities.
     *
     * @Route("/blog", name="blogarticle_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $blogArticleArray = $em->getRepository('AppBundle:BlogArticle')->findBy([],[
            'created' => 'desc'
        ]);

        return $this->render(':blog:index.html.twig', array(
            'blogArticleArray' => $blogArticleArray,
        ));
    }

    /**
     * Creates a new blogArticle entity.
     *
     * @Route("/new", name="blogarticle_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $blogArticle = new Blogarticle();
        $form = $this->createForm('AppBundle\Form\BlogArticleType', $blogArticle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($blogArticle);
            $em->flush($blogArticle);

            return $this->redirectToRoute('blogarticle_show', array('id' => $blogArticle->getId()));
        }

        return $this->render('blogarticle/new.html.twig', array(
            'blogArticle' => $blogArticle,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a blogArticle entity.
     *
     * @Route("/{id}", name="blogarticle_show")
     * @Method("GET")
     */
    public function showAction(BlogArticle $blogArticle)
    {
        $deleteForm = $this->createDeleteForm($blogArticle);

        return $this->render('blogarticle/show.html.twig', array(
            'blogArticle' => $blogArticle,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing blogArticle entity.
     *
     * @Route("/{id}/edit", name="blogarticle_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, BlogArticle $blogArticle)
    {
        $deleteForm = $this->createDeleteForm($blogArticle);
        $editForm = $this->createForm('AppBundle\Form\BlogArticleType', $blogArticle);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('blogarticle_edit', array('id' => $blogArticle->getId()));
        }

        return $this->render('blogarticle/edit.html.twig', array(
            'blogArticle' => $blogArticle,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a blogArticle entity.
     *
     * @Route("/{id}", name="blogarticle_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, BlogArticle $blogArticle)
    {
        $form = $this->createDeleteForm($blogArticle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($blogArticle);
            $em->flush($blogArticle);
        }

        return $this->redirectToRoute('blogarticle_index');
    }

    /**
     * Creates a form to delete a blogArticle entity.
     *
     * @param BlogArticle $blogArticle The blogArticle entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(BlogArticle $blogArticle)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('blogarticle_delete', array('id' => $blogArticle->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
