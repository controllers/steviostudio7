<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomepageController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $blogArticleArray = $em->getRepository('AppBundle:BlogArticle')->findBy([],[
            'created' => 'desc'
        ]);
        return $this->render(":homepage:index.html.twig", [
            'blogArticleArray' => $blogArticleArray,
        ]);
    }
}
