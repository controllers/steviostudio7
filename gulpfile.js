var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var del = require('del');
var Q = require('q');

var config = {
    assetsDir: 'app/Resources/assets',
    sassPattern: 'sass/**/*.scss',
    jsPattern: 'js/**/*.js',
    production: !!plugins.util.env.production,
    sourceMaps: !plugins.util.env.production,
    bowerDir: 'vendor/bower_components',
    bootstrapDir: 'vendor/bower_components/bootstrap',
    revManifestPath: 'app/Resources/assets/rev-manifest.json'
};
var app = {};

app.addStyle = function (paths, outputFilename) {
    return gulp.src(paths)
        .pipe(plugins.plumber())
        .pipe(plugins.if(config.sourceMaps, plugins.sourcemaps.init()))
        .pipe(plugins.sass({
            includePaths: [
                //config.bowerDir+'/foundation-sites/scss',
            ]
        }))
        .pipe(plugins.concat('css/' + outputFilename))
        .pipe(config.production ? plugins.minifyCss() : plugins.util.noop())
        .pipe(plugins.rev())
        .pipe(plugins.if(config.sourceMaps, plugins.sourcemaps.write('.')))
        .pipe(gulp.dest('web'))
        // write the rev-manifest.json file for gulp-rev
        .pipe(plugins.rev.manifest(config.revManifestPath, {
            merge: true
        }))
        .pipe(gulp.dest('.'));
};
app.addFolderContentsVers = function (paths, outputDir) {
    return gulp.src(paths, {base: config.assetsDir})
        .pipe(plugins.rev())
        .pipe(gulp.dest('web/' + outputDir))
        // write the rev-manifest.json file for gulp-rev
        .pipe(plugins.rev.manifest(config.revManifestPath, {
            merge: true
        }))
        .pipe(gulp.dest('.'));
};

app.addScript = function (paths, outputFilename) {
    return gulp.src(paths)
        .pipe(plugins.plumber())
        .pipe(plugins.if(config.sourceMaps, plugins.sourcemaps.init()))
        .pipe(plugins.concat('js/' + outputFilename))
        .pipe(config.production ? plugins.uglify() : plugins.util.noop())
        .pipe(plugins.rev())
        .pipe(plugins.if(config.sourceMaps, plugins.sourcemaps.write('.')))
        .pipe(gulp.dest('web'))
        // write the rev-manifest.json file for gulp-rev
        .pipe(plugins.rev.manifest(config.revManifestPath, {
            merge: true
        }))
        .pipe(gulp.dest('.'));
};

app.copy = function (srcFiles, outputDir) {
    gulp.src(srcFiles)
        .pipe(gulp.dest(outputDir));
};

var Pipeline = function () {
    this.entries = [];
};

Pipeline.prototype.add = function () {
    this.entries.push(arguments);
};

Pipeline.prototype.run = function (callable) {
    var deferred = Q.defer();
    var i = 0;
    var entries = this.entries;
    var runNextEntry = function () {
        // see if we're all done looping
        if (typeof entries[i] === 'undefined') {
            deferred.resolve();
            return;
        }
        // pass app as this, though we should avoid using "this"
        // in those functions anyways
        callable.apply(app, entries[i]).on('end', function () {
            i++;
            runNextEntry();
        });
    };
    runNextEntry();
    return deferred.promise;
};

gulp.task('styles', function () {
    var pipeline = new Pipeline();
    pipeline.add([
        config.bootstrapDir + '/dist/css/bootstrap.min.css',
        config.bootstrapDir + '/dist/css/bootstrap-theme.min.css',
        config.bowerDir + '/font-awesome/css/font-awesome.css',
        config.bowerDir + '/animate.css/animate.min.css',
        config.assetsDir + '/sass/steviostudio7.scss',
    ], 'steviostudio7.css');
    pipeline.run(app.addStyle);
    //Copio le immagini necessarie ad alcuni css
    app.copy(config.bootstrapDir + '/dist/css/bootstrap.min.css.map', 'web/css');
});

gulp.task('img-vers', function () {
    var pipeline = new Pipeline();
    pipeline.add([
        config.assetsDir + '/img-vers/*'
    ], '.');
    pipeline.add([
        config.assetsDir + '/favicons/*'
    ], '.');
    pipeline.run(app.addFolderContentsVers);
});

gulp.task('scripts', function () {
    var pipeline = new Pipeline();
    pipeline.add([
        config.bowerDir + '/jquery/dist/jquery.min.js',
        config.bowerDir + '/jquery-throttle-debounce/jquery.ba-throttle-debounce.min.js',
        config.bootstrapDir + '/dist/js/bootstrap.min.js',
        //config.bowerDir + '/foundation-sites/dist/foundation.js',
        config.assetsDir + '/js/main.js'
    ], 'site.js');
    pipeline.run(app.addScript);
});

gulp.task('clean', function () {
    del.sync(config.revManifestPath);
    del.sync('web/css/*');
    del.sync('web/js/*')
    del.sync('web/img-vers/*');
});

gulp.task('fonts', function () {
    app.copy(
        config.bowerDir + '/font-awesome/fonts/*',
        'web/fonts'
    );
});

gulp.task('watch', function () {
    gulp.watch(config.assetsDir + '/' + config.sassPattern, ['styles']);
    gulp.watch(config.assetsDir + '/' + config.jsPattern, ['scripts']);
});

gulp.task('default', ['clean', 'styles', 'scripts', 'img-vers', 'watch']);